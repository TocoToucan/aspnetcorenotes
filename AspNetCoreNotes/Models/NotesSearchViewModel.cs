﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreNotes.Models
{
    public class NotesSearchViewModel
    {
        public string Filter { get; set; }
    }
}
