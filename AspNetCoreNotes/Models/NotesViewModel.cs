﻿using System.Collections.Generic;

namespace AspNetCoreNotes.Models
{
    public class NotesViewModel
    {
        public List<Note> Notes { get; set; }
    }
}