﻿using System.Threading.Tasks;
using AspNetCoreNotes.Models;
using AspNetCoreNotes.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreNotes.Controllers
{
    [Authorize]
    public class NotesController : Controller
    {
        private readonly INotesService _notesService;
        private readonly UserManager<IdentityUser> _userManager;

        public NotesController(INotesService notesService, UserManager<IdentityUser> userManager)
        {
            _notesService = notesService;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(string filter)
        {
            var user = await _userManager.GetUserAsync(User);
            var notes = await _notesService.GetNotesAsync(user.Id, filter);

            var notesViewModel = new NotesViewModel
            {
                Notes = notes
            };

            return View(notesViewModel);
        }

        public async Task<IActionResult> Details(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var note = await _notesService.GetNoteAsync(id);

            ValidateNote(note, user);

            return View(note);
        }

        public IActionResult Create()
        {
            return View(new Note());
        }

        public async Task<IActionResult> Add(Note note)
        {
            note.User = await _userManager.GetUserAsync(User);
            await _notesService.AddAsync(note);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var note = await _notesService.GetNoteAsync(id);

            ValidateNote(note, user);

            await _notesService.DeleteAsync(note);
            return RedirectToAction(nameof(Index));
        }

        private void ValidateNote(Note note, IdentityUser user)
        {
            if (note == null)
                NotFound();

            if (note.User != user)
                Forbid();
        }
    }
}