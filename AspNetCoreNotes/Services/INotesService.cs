﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AspNetCoreNotes.Models;

namespace AspNetCoreNotes.Services
{
    public interface INotesService
    {
        Task AddAsync(Note note);
        Task DeleteAsync(Note id);
        Task<List<Note>> GetNotesAsync(string userId, string filter = null);
        Task<Note> GetNoteAsync(int id);
    }
}