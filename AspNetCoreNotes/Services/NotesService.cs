﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreNotes.Data;
using AspNetCoreNotes.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreNotes.Services
{
    public class NotesService : INotesService
    {
        private readonly IAvatarService _avatarService;
        private readonly ApplicationDbContext _context;

        public NotesService(IAvatarService avatarService, ApplicationDbContext context)
        {
            _avatarService = avatarService;
            _context = context;
        }

        public async Task AddAsync(Note note)
        {
            note.CreatedAt = DateTimeOffset.Now;
            note.Identicon = await _avatarService.GetAsync();

            await _context.Notes.AddAsync(note);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Note note)
        {
            _context.Notes.Remove(note);
            await _context.SaveChangesAsync();
        }

        public async Task<List<Note>> GetNotesAsync(string userId, string filter = null)
        {
            var shouldFilter = !string.IsNullOrEmpty(filter);

            var result = _context.Notes.Include(note => note.User).Where(note => note.User.Id == userId);

            if (shouldFilter)
                result = result.Where(note => note.Title.IndexOf(filter, StringComparison.Ordinal) >= 0);

            return await result.ToListAsync();
        }

        public Task<Note> GetNoteAsync(int id)
        {
            return _context.Notes.FirstOrDefaultAsync(n => n.Id == id);
        }
    }
}