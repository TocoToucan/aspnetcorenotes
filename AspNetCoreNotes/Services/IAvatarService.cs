﻿using System.Threading.Tasks;

namespace AspNetCoreNotes.Services
{
    public interface IAvatarService
    {
        Task<string> GetAsync(string id = null);
    }
}
