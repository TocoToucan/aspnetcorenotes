﻿using System;
using System.Threading.Tasks;

namespace AspNetCoreNotes.Services
{
    public class IdenticonService : IAvatarService
    {
        public Task<string> GetAsync(string id = null)
        {
            if (id == null)
                id = Guid.NewGuid().ToString();

            return Task.FromResult($"https://avatars.dicebear.com/v2/identicon/{id}.svg");
        }
    }
}