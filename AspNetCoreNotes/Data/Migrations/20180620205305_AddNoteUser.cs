﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreNotes.Data.Migrations
{
    public partial class AddNoteUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Notes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Notes_UserId",
                table: "Notes",
                column: "UserId");

            // see https://github.com/nbarbettini/little-aspnetcore-book/blob/master/chapters/use-a-database/create-migration.md

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Notes_AspNetUsers_UserId",
            //    table: "Notes",
            //    column: "UserId",
            //    principalTable: "AspNetUsers",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // see https://github.com/nbarbettini/little-aspnetcore-book/blob/master/chapters/use-a-database/create-migration.md

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Notes_AspNetUsers_UserId",
            //    table: "Notes");

            migrationBuilder.DropIndex(
                name: "IX_Notes_UserId",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Notes");
        }
    }
}
