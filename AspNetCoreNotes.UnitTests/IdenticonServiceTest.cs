using System;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreNotes.Services;
using Xunit;

namespace AspNetCoreNotes.UnitTests
{
    public class IdenticonServiceTest
    {
        private readonly IdenticonService _sut = new IdenticonService();

        [Fact]
        public async Task GetConcreteIdenticon()
        {
            var id = "test";
            var expectedResult = $"https://avatars.dicebear.com/v2/identicon/{id}.svg";

            var actualResult = await _sut.GetAsync(id);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public async Task GetRandomIdenticon()
        {
            var actualResult = await _sut.GetAsync();

            // We expect System.Guid here
            var id = actualResult.Split('/').Last().Split('.').First();

            Assert.Equal(Guid.Empty.ToString().Length, id.Length);
            Assert.Equal(Guid.Empty.ToString().Split('-').Length, id.Split('-').Length);
        }
    }
}