﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreNotes.Data;
using AspNetCoreNotes.Models;
using AspNetCoreNotes.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AspNetCoreNotes.UnitTests
{
    public class NotesServiceTest
    {
        private readonly IdenticonService _identiconService = new IdenticonService();

        private DbContextOptions<ApplicationDbContext> GetDbContextOptions(string dbName)
        {
            return new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(dbName).Options;
        }

        private List<Note> GetMyNotes()
        {
            var user = new IdentityUser
            {
                Id = "me",
                UserName = "me@example.com"
            };

            return new List<Note>
            {
                new Note
                {
                    Title = "First note",
                    Body = "First note content",
                    User = user
                },
                new Note
                {
                    Title = "Second note",
                    Body = "Second note content",
                    User = user
                }
            };
        }

        private Note GetNotMyNote()
        {
            return new Note
            {
                Title = "Not my note",
                Body = "Not my content",
                User = new IdentityUser
                {
                    Id = "not_me",
                    UserName = "not_me@example.com"
                }
            };
        }

        [Fact]
        public async Task AddNewNote()
        {
            var addNewNoteContextOptions = GetDbContextOptions($"{nameof(AddNewNote)}.db");
            var note = GetMyNotes().First();

            // Set up a context (connection to the "DB") for writing
            using (var context = new ApplicationDbContext(addNewNoteContextOptions))
            {
                var service = new NotesService(_identiconService, context);

                await service.AddAsync(note);
            }

            // Use a separate context to read data back from the "DB"
            using (var context = new ApplicationDbContext(addNewNoteContextOptions))
            {
                var service = new NotesService(_identiconService, context);

                var notesFromDb = await service.GetNotesAsync(note.User.Id);
                Assert.Single(notesFromDb);

                var noteFromDb = notesFromDb.Single();

                Assert.Equal(note.Title, noteFromDb.Title);
                Assert.Equal(note.Body, noteFromDb.Body);
                Assert.Equal(note.User.Id, noteFromDb.User.Id);

                var difference = DateTimeOffset.Now - noteFromDb.CreatedAt;
                Assert.True(difference < TimeSpan.FromSeconds(5));
            }
        }

        [Fact]
        public async Task DeleteNote()
        {
            var deleteNoteContextOptions = GetDbContextOptions($"{nameof(DeleteNote)}.db");
            var note = GetMyNotes().First();

            using (var context = new ApplicationDbContext(deleteNoteContextOptions))
            {
                var service = new NotesService(_identiconService, context);

                await service.AddAsync(note);

                var notesFromDb = await service.GetNotesAsync(note.User.Id);
                Assert.Single(notesFromDb);

                await service.DeleteAsync(notesFromDb.Single());
                Assert.Empty(await service.GetNotesAsync(note.User.Id));
            }
        }

        [Fact]
        public async Task GetNoteById()
        {
            var getNoteByIdContextOptions = GetDbContextOptions($"{nameof(GetNoteById)}.db");

            var myNotes = GetMyNotes();

            using (var context = new ApplicationDbContext(getNoteByIdContextOptions))
            {
                var service = new NotesService(_identiconService, context);

                foreach (var myNote in myNotes) await service.AddAsync(myNote);

                var myNotesFromDb = await service.GetNotesAsync(myNotes.First().User.Id);
                var myFirstNoteFromDb = myNotesFromDb.First();

                var myNoteById = await service.GetNoteAsync(myFirstNoteFromDb.Id);

                Assert.Equal(myFirstNoteFromDb.Id, myNoteById.Id);
                Assert.Equal(myFirstNoteFromDb.Title, myNoteById.Title);
                Assert.Equal(myFirstNoteFromDb.Body, myNoteById.Body);
                Assert.Equal(myFirstNoteFromDb.User.Id, myNoteById.User.Id);
            }
        }

        [Fact]
        public async Task GetNotes()
        {
            var getNotesContextOptions = GetDbContextOptions($"{nameof(GetNotes)}.db");

            var myNotes = GetMyNotes();
            var notMyNote = GetNotMyNote();

            using (var context = new ApplicationDbContext(getNotesContextOptions))
            {
                var service = new NotesService(_identiconService, context);

                foreach (var note in myNotes.Union(new List<Note> {notMyNote})) await service.AddAsync(note);

                var myNotesFromDb = await service.GetNotesAsync(myNotes.First().User.Id);
                var notMyNotesFromDb = await service.GetNotesAsync(notMyNote.User.Id);

                Assert.Equal(2, myNotesFromDb.Count);
                Assert.Single(notMyNotesFromDb);

                var myFilteredNotesFromDb = await service.GetNotesAsync(myNotes.First().User.Id, "Fi");
                Assert.Single(myFilteredNotesFromDb);

                var myFilteredNoteFromDb = myFilteredNotesFromDb.Single();
                Assert.Equal(myNotes.First().Title, myFilteredNoteFromDb.Title);
                Assert.Equal(myNotes.First().Body, myFilteredNoteFromDb.Body);
                Assert.Equal(myNotes.First().User.Id, myFilteredNoteFromDb.User.Id);
            }
        }
    }
}